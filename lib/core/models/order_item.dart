class OrderItem {
  int id;
  String title;
  String price;
  String url;

  OrderItem(this.id, this.title, this.price, this.url);
}