import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';

class AuthenticationService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<FirebaseUser> signIn(String email, String password) async {
    final AuthCredential credential =
        EmailAuthProvider.getCredential(email: email, password: password);

    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;

    print(user.displayName);
    return user;
  }

  Future<FirebaseUser> signInWithFacebook(String accessToken) async {
    final AuthCredential fbCred =
        FacebookAuthProvider.getCredential(accessToken: accessToken);
    final FirebaseUser user = (await _auth.signInWithCredential(fbCred)).user;

    return user;
  }

  Future<FirebaseUser> signInWithCredential(AuthCredential credential) async {
    return (await _auth.signInWithCredential(credential)).user;
  }

  Future<FirebaseUser> register(String email, String password) async {
    final FirebaseUser user = (await _auth.createUserWithEmailAndPassword(
            email: email, password: password))
        .user;

    return user;
  }

  Future<FirebaseUser> currentUser() async {
    return _auth.currentUser();
  }
}
