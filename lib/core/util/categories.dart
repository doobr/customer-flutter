import 'package:flutter/material.dart';

List categories = [
  {
    "name": "Flower",
    "color1": Color.fromARGB(100, 0, 0, 0),
    "color2": Color.fromARGB(100, 0, 0, 0),
    "img": "assets/images/categories/flower.jpg"
  },
  {
    "name": "Edibles",
    "color1": Color.fromARGB(100, 0, 0, 0),
    "color2": Color.fromARGB(100, 0, 0, 0),
    "img": "assets/images/categories/edible.jpeg"
  },
  {
    "name": "Concentrates",
    "color1": Color.fromARGB(100, 0, 0, 0),
    "color2": Color.fromARGB(100, 0, 0, 0),
    "img": "assets/images/categories/concentrate.jpg"
  },
  {
    "name": "Cartridge",
    "color1": Color.fromARGB(100, 0, 0, 0),
    "color2": Color.fromARGB(100, 0, 0, 0),
    "img": "assets/images/categories/cartridge.jpg"
  },
  {
    "name": "Preroll",
    "color1": Color.fromARGB(100, 0, 0, 0),
    "color2": Color.fromARGB(100, 0, 0, 0),
    "img": "assets/images/categories/preroll.jpg"
  },
  {
    "name": "Topical",
    "color1": Color.fromARGB(100, 0, 0, 0),
    "color2": Color.fromARGB(100, 0, 0, 0),
    "img": "assets/images/categories/topical.jpg"
  },
  {
    "name": "Accessory",
    "color1": Color.fromARGB(100, 0, 0, 0),
    "color2": Color.fromARGB(100, 0, 0, 0),
    "img": "assets/images/categories/accessory.jpg"
  },
];