List dispensaries = [
  {
    "img": "assets/images/dispensaries/medmar.jpg",
    "title": "MedMar - Chicago",
    "address": "1278 Loving Acres Road Chicago, IL 60605",
    "rating": "4.7"
  },
  {
    "img": "assets/images/dispensaries/floramed.jpg",
    "title": "FloraMed",
    "address": "1278 Loving Acres Road Chicago, IL 60605",
    "rating": "4.2"
  },
  {
    "img": "assets/images/dispensaries/rise.jpg",
    "title": "Rise Health",
    "address": "1278 Loving Acres Road Chicago, IL 60605",
    "rating": "3.8"
  },
  {
    "img": "assets/images/dispensaries/earthmed.jpg",
    "title": "Earth Meds",
    "address": "1278 Loving Acres Road Chicago, IL 60605",
    "rating": "4.2"
  },
  {
    "img": "assets/images/dispensaries/floramed.jpg",
    "title": "FloraMed",
    "address": "1278 Loving Acres Road Chicago, IL 60605",
    "rating": "4.8"
  },
  {
    "img": "assets/images/dispensaries/rise.jpg",
    "title": "Rise Health",
    "address": "1278 Loving Acres Road Chicago, IL 60605",
    "rating": "4.9"
  },
  {
    "img": "assets/images/dispensaries/earthmed.jpg",
    "title": "Earth Meds",
    "address": "1278 Loving Acres Road Chicago, IL 60605",
    "rating": "4.5"
  },
  {
    "img": "assets/images/dispensaries/medmar.jpg",
    "title": "MedMar - Chicago",
    "address": "1278 Loving Acres Road Chicago, IL 60605",
    "rating": "4.2"
  },
  {
    "img": "assets/images/dispensaries/rise.jpg",
    "title": "Rise Health",
    "address": "1278 Loving Acres Road Chicago, IL 60605",
    "rating": "4.1"
  }
];