import 'package:flutter/material.dart';
import 'package:doobr_customer/provider_setup.dart';
import 'package:doobr_customer/ui/router.dart';
import 'package:provider/provider.dart';

import 'core/constants/app_contstants.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers,
      child: MaterialApp(
        title: 'Doobr',
        darkTheme: ThemeData.dark(),
        theme: ThemeData.light(),
        initialRoute: RoutePaths.Login,
        onGenerateRoute: Router.generateRoute,
      ),
    );
  }
}
