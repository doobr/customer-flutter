import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'core/services/api.dart';
import 'core/services/authentication_service.dart';

List<SingleChildStatelessWidget> providers = [
  ...independentServices,
  ...dependentServices,
  ...uiConsumableProviders
];

List<SingleChildStatelessWidget> independentServices = [
  Provider.value(value: Api())
];
List<SingleChildStatelessWidget> dependentServices = [
  ProxyProvider<Api, AuthenticationService>(
      update: (context, api, authenticationService) =>
          AuthenticationService())
];
List<SingleChildStatelessWidget> uiConsumableProviders = [
  // StreamProvider<User>(
  //     create: (context) =>
  //         Provider.of<AuthenticationService>(context, listen: false).user)
];
