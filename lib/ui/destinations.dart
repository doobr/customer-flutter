import 'package:flutter/material.dart';

class Destination {
  const Destination(this.title, this.icon);
  final String title;
  final IconData icon;
}

const List<Destination> allDestinations = <Destination>[
  Destination('Home', Icons.home),
  Destination('Search', Icons.search),
  Destination('My Orders', Icons.history),
  Destination('Profile', Icons.portrait)
];