import 'package:doobr_customer/ui/views/login_page.dart';
import 'package:doobr_customer/ui/views/main_view.dart';
import 'package:doobr_customer/ui/views/signup_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:doobr_customer/core/constants/app_contstants.dart';
import 'package:doobr_customer/core/models/post.dart';
import 'package:doobr_customer/ui/views/post_view.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RoutePaths.Home:
        return MaterialPageRoute(builder: (_) => MainView());
      case RoutePaths.Login:
        return MaterialPageRoute(builder: (_) => LoginPage());
      case RoutePaths.Post:
        var post = settings.arguments as Post;
        return MaterialPageRoute(builder: (_) => PostView(post: post));
      case RoutePaths.SignUp:
        return MaterialPageRoute(builder: (_) => SignUpView());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }
}
