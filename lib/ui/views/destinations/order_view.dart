import 'package:doobr_customer/ui/destinations.dart';
import 'package:doobr_customer/ui/shared/app_colors.dart';
import 'package:doobr_customer/ui/shared/text_styles.dart';
import 'package:doobr_customer/ui/shared/ui_helpers.dart';
import 'package:doobr_customer/ui/widgets/core/base_widget.dart';
import 'package:doobr_customer/viewmodels/views/order_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';

class OrderView extends StatefulWidget {
  const OrderView({Key key, this.destination}) : super(key: key);

  final Destination destination;

  @override
  _OrderViewViewState createState() => _OrderViewViewState();
}

class _OrderViewViewState extends State<OrderView> {
  Position _position;


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<OrderViewModel>(
        onModelReady: (model) {
          model.getLocation().then((position) => {_position = position});
        },
        model: OrderViewModel(),
        child: Scaffold(),
        builder: (context, model, child) => Scaffold(
              backgroundColor: backgroundColor,
              body: model.busy
                  ? Center(child: SpinKitWave(color: Colors.lightGreen))
                  : Scaffold(
                      appBar: AppBar(
                        title: Text('Order a Doobr'),
                        backgroundColor: Colors.lightGreen,
                      ),
                      backgroundColor: Colors.white,
                      body: Center(
                        child: Text("Order Screen")
                      )
                    ),
            ));
  }
}
