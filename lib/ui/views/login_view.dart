import 'package:doobr_customer/core/constants/app_contstants.dart';
import 'package:doobr_customer/ui/shared/text_styles.dart';
import 'package:doobr_customer/ui/shared/ui_helpers.dart';
import 'package:doobr_customer/ui/widgets/auth/login_form.dart';
import 'package:doobr_customer/ui/widgets/core/base_widget.dart';
import 'package:doobr_customer/viewmodels/views/login_viewmodel.dart';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:doobr_customer/ui/shared/app_colors.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<LoginViewModel>(
      onModelReady: (model) {
        model.isUserTokenValid().then((userValid) =>
            {Navigator.pushReplacementNamed(context, RoutePaths.Home)});
      },
      model: LoginViewModel(Provider.of(context)),
      child: LoginForm(),
      builder: (context, model, child) => Scaffold(
        backgroundColor: backgroundColor,
        body: model.busy
            ? 
            Center(child: SpinKitWave(color: Colors.lightGreen))
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  UIHelper.verticalSpaceMedium,
                  Text('Log In', style: headerStyle),
                  LoginForm(),
                  model.busy
                      ? Row()
                      : FlatButton(
                          color: Colors.grey,
                          child: Text('Sign Up',
                              style: TextStyle(color: Colors.black)),
                          onPressed: () async {
                            Navigator.pushNamed(context, RoutePaths.SignUp);
                          },
                        )
                ],
              ),
      ),
    );
  }
}
