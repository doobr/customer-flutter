import 'package:doobr_customer/ui/destinations.dart';
import 'package:doobr_customer/ui/views/destinations/order_view.dart';
import 'package:doobr_customer/ui/views/destinations/profile_view.dart';
import 'package:doobr_customer/ui/views/home/home_view.dart';
import 'package:doobr_customer/ui/views/my_orders/my_orders.dart';
import 'package:doobr_customer/ui/views/search/search_view.dart';
import 'package:flutter/material.dart';

class MainView extends StatefulWidget {
  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: false,
        child: IndexedStack(
          index: _currentIndex,
          children: allDestinations.map<Widget>((Destination destination) {
            switch(destination.title) {
              case "Home":
              return HomeView();
                break;
              case "Search": 
                return SearchView();
                break;
              case "My Orders":
                return MyOrders(destination: destination);
                break;
              case "Profile":
                return ProfileView(destination: destination);
                break;
              default:
                return OrderView(destination: destination);
                break;
            }
          }).toList(),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.shifting,
        elevation: 2.0,
        backgroundColor: Colors.white,
        onTap: (int index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: allDestinations.map((Destination destination) {
          return BottomNavigationBarItem(
            icon: Icon(destination.icon, color: Colors.lightGreen,),
            title: Text(destination.title, style: TextStyle(color: Colors.lightGreen),)
          );
        }).toList(),
      ),
    );
  }
}
