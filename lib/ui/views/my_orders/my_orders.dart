import 'dart:ui';

import 'package:doobr_customer/core/constants/app_contstants.dart';
import 'package:doobr_customer/core/models/order_item.dart';
import 'package:doobr_customer/ui/destinations.dart';
import 'package:doobr_customer/ui/widgets/my_orders/order_bottom_sheet.dart';
import 'package:doobr_customer/ui/widgets/my_orders/order_item_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyOrders extends StatefulWidget {
  const MyOrders({Key key, this.destination}) : super(key: key);

  final Destination destination;

  @override
  _MyOrdersViewState createState() => _MyOrdersViewState();
}

class _MyOrdersViewState extends State<MyOrders> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();

  List<OrderItemView> _data = [
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(2, "Super Silver Haze 3.5g", "\$64.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(3, "Green Crack 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(4, "1g Concentrate", "\$129.99",
        "https://reno.thedispensarynv.com/wp-content/uploads/2019/06/OG-Bubblegum.jpeg")),
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
    OrderItemView(OrderItem(1, "Blue Dream 7g", "\$129.99",
        "https://buyonlineweeds.com/wp-content/uploads/2018/08/blue_dream.jpg")),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 2.0,
          backgroundColor: Colors.white,
          title: Text('My Orders',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 30.0)),
        ),
        backgroundColor: Colors.white,
        body: ListView.builder(
            key: _listKey,
            itemCount: _data.length,
            itemBuilder: (ctx, index) {
              return GestureDetector(
                  onTap: () {
                    showModalBottomSheet(
                        context: ctx,
                        isScrollControlled: true,
                        builder: ((ctx) =>
                            OrderBottomSheet(_data[index].item)));
                  },
                  child: Card(child: _data[index]));
            }));
  }
}
