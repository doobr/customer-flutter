import 'package:doobr_customer/ui/shared/text_styles.dart';
import 'package:doobr_customer/ui/shared/ui_helpers.dart';
import 'package:doobr_customer/ui/widgets/auth/sign_up_form.dart';
import 'package:doobr_customer/ui/widgets/core/base_widget.dart';
import 'package:doobr_customer/viewmodels/views/sign_up_viewmodel.dart';

import '../../core/constants/app_contstants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:doobr_customer/ui/shared/app_colors.dart';
import '../../core/services/authentication_service.dart';

class SignUpView extends StatefulWidget {
  @override
  _SignUpViewState createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SignUpViewModel>(
      model: SignUpViewModel(Provider.of(context)),
      child: SignUpForm(),
      builder: (context, model, child) => Scaffold(
        backgroundColor: backgroundColor,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            UIHelper.verticalSpaceMedium,
            Text('Sign Up', style: headerStyle),
            SignUpForm(),
            FlatButton(
              color: Colors.grey,
              child: Text('Back', style: TextStyle(color: Colors.black)),
              onPressed: () async {
                Navigator.pushNamedAndRemoveUntil(
                    context, RoutePaths.Login, (r) => false);
              },
            )
          ],
        ),
      ),
    );
  }
}
