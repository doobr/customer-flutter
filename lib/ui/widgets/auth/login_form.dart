import 'package:doobr_customer/core/constants/app_contstants.dart';
import 'package:doobr_customer/viewmodels/views/login_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:doobr_customer/ui/widgets/core/base_widget.dart';
import 'package:provider/provider.dart';

class LoginForm extends StatefulWidget {
  LoginForm();

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  String email = "";
  String password = "";

  bool _isEmail(String em) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }

  bool _isValidPassword(String em) {
    return em.length > 5;
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<LoginViewModel>(
        model: LoginViewModel(Provider.of(context)),
        child: null,
        builder: (context, model, child) => Form(
              key: _formKey,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 24.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: "Email",
                        hasFloatingPlaceholder: true,
                      ),
                      onChanged: (value) {
                        email = value;
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter your email.';
                        }
                        if (!_isEmail(value)) {
                          return 'Email address not valid.';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      obscureText: true,
                      keyboardType: TextInputType.visiblePassword,
                      decoration: InputDecoration(
                        labelText: "Password",
                        hasFloatingPlaceholder: true,
                      ),
                      onChanged: (value) {
                        password = value;
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter your password.';
                        }
                        if (!_isValidPassword(value)) {
                          return 'Password must be at least 6 characters.';
                        }
                        return null;
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: RaisedButton(
                        color: Colors.lightGreen,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            model.emailLogin(email, password).then((worked) => {
                              if (worked != null) {
                                Navigator.pushNamed(context, RoutePaths.Home)
                              }
                            });
                          }
                        },
                        child: Text('Log In'),
                      ),
                    )
                  ],
                ),
              ),
            ));
  }
}
