import 'package:doobr_customer/core/constants/app_contstants.dart';
import 'package:doobr_customer/viewmodels/views/sign_up_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:doobr_customer/ui/widgets/core/base_widget.dart';

class SignUpForm extends StatefulWidget {
  SignUpForm();

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  String _email = "";
  String _password = "";

  bool _isEmail(String em) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }

  bool _isValidPassword(String em) {
    return em.length > 5;
  }

  bool _passwordsMatch(String em) {
    return em == _password;
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SignUpViewModel>(
        model: SignUpViewModel(Provider.of(context)),
        child: null,
        builder: (context, model, child) => Form(
              key: _formKey,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 24.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: "Email",
                        hasFloatingPlaceholder: true,
                      ),
                      onChanged: (value) {
                        _email = value;
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter your email.';
                        }
                        if (!_isEmail(value)) {
                          return 'Email address not valid.';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      onChanged: (value) {
                        _password = value;
                      },
                      obscureText: true,
                      keyboardType: TextInputType.visiblePassword,
                      decoration: InputDecoration(
                        labelText: "Password",
                        hasFloatingPlaceholder: true,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter your password.';
                        }
                        if (!_isValidPassword(value)) {
                          return 'Password must be at least 6 characters.';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      obscureText: true,
                      keyboardType: TextInputType.visiblePassword,
                      decoration: InputDecoration(
                        labelText: "Confirm Password",
                        hasFloatingPlaceholder: true,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please confirm your password.';
                        }
                        if (!_passwordsMatch(value)) {
                          return 'Passwords do not match.';
                        }
                        return null;
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: 
                        model.busy 
                        ? CircularProgressIndicator()
                        : RaisedButton(
                        color: Colors.lightGreen,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            model.signUp(_email, _password).then((user) => {
                              Navigator.pushReplacementNamed(context, RoutePaths.Home)
                            });
                          }
                        },
                        child: Text('Register'),
                      ),
                    )
                  ],
                ),
              ),
            ));
  }
}
