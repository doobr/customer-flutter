import 'package:doobr_customer/viewmodels/wigets/comments_model.dart';
import 'package:flutter/material.dart';
import 'package:doobr_customer/core/models/comment.dart';
import 'package:doobr_customer/ui/widgets/core/base_widget.dart';
import 'package:provider/provider.dart';

class Comments extends StatelessWidget {
  final int postId;
  Comments(this.postId);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

/// Renders a single comment given a comment model
class CommentItem extends StatelessWidget {
  final Comment comment;
  const CommentItem(this.comment);

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CommentsModel>(
        onModelReady: (model) => model.fetchComments(comment.postId),
        model: CommentsModel(Provider.of(context)),
        builder: (context, model, child) => model.busy
            ? Center(child: CircularProgressIndicator())
            : Expanded(
                child: ListView.builder(
                  itemCount: model.comments.length,
                  itemBuilder: (context, index) =>
                      CommentItem(model.comments[index]),
                ),
              ));
  }
}
