import 'package:cached_network_image/cached_network_image.dart';
import 'package:doobr_customer/core/models/order_item.dart';
import 'package:doobr_customer/ui/shared/ui_helpers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class OrderBottomSheet extends StatelessWidget {
  final OrderItem _item;
  OrderBottomSheet(this._item, {GlobalKey key}) : super(key: key);

  // set up the buttons
  showAlertDialog(BuildContext context) {
    AlertDialog alert;
    Widget continueButton = FlatButton(
      child: Text("Okay"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    alert = AlertDialog(
      title: Text("Order Placed"),
      content: Text("You reordered ${_item.title}"),
      actions: [
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * .9,
      child: Material(
        clipBehavior: Clip.antiAlias,
        elevation: 2.0,
        borderRadius: BorderRadius.circular(4.0),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FlatButton(
                    child: Icon(Icons.close),
                    onPressed: () {
                      Navigator.maybePop(context);
                    },
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Expanded(
                      child: CachedNetworkImage(
                    imageUrl: this._item.url,
                    placeholder: (context, url) => SpinKitWave(
                      color: Colors.lightGreen,
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  )),
                ],
              ),
              ListTile(
                title: Text("Date"),
                subtitle: Text("26 June 2018"),
                trailing: Text("11:00 AM"),
              ),
              ListTile(
                title: Text("Pawan Kumar"),
                subtitle: Text("mtechviral@gmail.com"),
                trailing: CircleAvatar(
                  radius: 20.0,
                  backgroundImage: NetworkImage(
                      "https://avatars0.githubusercontent.com/u/12619420?s=460&v=4"),
                ),
              ),
              ListTile(
                title: Text("Amount"),
                subtitle: Text("\$299"),
                trailing: Text("Completed"),
              ),
              Card(
                clipBehavior: Clip.antiAlias,
                elevation: 0.0,
                color: Colors.grey.shade300,
                child: ListTile(
                  leading: Icon(
                    Icons.credit_card,
                    color: Colors.blue,
                  ),
                  title: Text("Credit/Debit Card"),
                  subtitle: Text("Amex Card ending ***6"),
                ),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: RaisedButton(
                      color: Colors.lightGreen,
                      onPressed: () {
                        showAlertDialog(context);
                      },
                      child: Text('Order Again'),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
