import 'package:doobr_customer/core/models/order_item.dart';
import 'package:doobr_customer/ui/shared/ui_helpers.dart';
import 'package:flutter/material.dart';

class OrderItemView extends StatefulWidget {
  final OrderItem item;
  OrderItemView(this.item);

  @override
  _OrderItemState createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItemView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(widget.item.title),
                UIHelper.horizontalSpaceMedium,
                Text(widget.item.price)
              ],
            ),
        ],
      ),
    );
  }
}
