import 'package:doobr_customer/core/constants/app_contstants.dart';
import 'package:doobr_customer/core/models/user.dart';
import 'package:doobr_customer/ui/widgets/postlist_item.dart';
import 'package:flutter/material.dart';
import 'package:doobr_customer/ui/widgets/core/base_widget.dart';
import 'package:doobr_customer/viewmodels/wigets/posts_model.dart';
import 'package:provider/provider.dart';

class Posts extends StatelessWidget {
  const Posts({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget<PostsModel>(
        model: PostsModel(Provider.of(context)),
        onModelReady: (model) => model.getPosts(Provider.of<User>(context).id),
        builder: (context, model, child) => model.busy
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: model.posts.length,
                itemBuilder: (context, index) => PostListItem(
                  post: model.posts[index],
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      RoutePaths.Post,
                      arguments: model.posts[index],
                    );
                  },
                ),
              ));
  }
}
