import 'package:doobr_customer/viewmodels/base_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../../core/services/authentication_service.dart';
import 'dart:developer' as developer;

class LoginViewModel extends BaseModel {
  AuthenticationService _authenticationService;

  LoginViewModel(this._authenticationService);

  Future<FirebaseUser> signUp(String email, String password) async {
    setBusy(true);
    var user = await _authenticationService.register(email, password);
    setBusy(false);
    return user;
  }

  Future<bool> isUserTokenValid() async {
    setBusy(true);
    FirebaseUser currentUser = await _authenticationService.currentUser();
    String token = "";
    await currentUser
        .getIdToken(refresh: true)
        .then((user) => {token = user.token});
    setBusy(false);
    return token != null;
  }

  Future<FirebaseUser> facebookSignin() async {
    setBusy(true);
    final facebookLogin = FacebookLogin();
    final result = await facebookLogin.logIn(['email']);
    FirebaseUser currentUser;
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        currentUser = await _authenticationService
            .signInWithFacebook(result.accessToken.token);
        break;
      case FacebookLoginStatus.cancelledByUser:
        // TODO Handle Cancelled
        break;
      case FacebookLoginStatus.error:
        // TODO Handle Error
        break;
    }
    setBusy(false);
    return currentUser;
  }

  Future<FirebaseUser> googleSignIn() async {
    setBusy(true);
    final GoogleSignIn _googleSignIn = GoogleSignIn();
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final FirebaseUser currentUser =
        await _authenticationService.signInWithCredential(credential);
    setBusy(false);

    return currentUser;
  }

  Future<FirebaseUser> emailLogin(String email, String password) async {
    setBusy(true);
    var user = await _authenticationService.signIn(email, password);
    setBusy(false);

    return user;
  }
}
