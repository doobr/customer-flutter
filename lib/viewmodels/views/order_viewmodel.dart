import 'package:doobr_customer/viewmodels/base_model.dart';
import 'package:geolocator/geolocator.dart';

class OrderViewModel extends BaseModel {

  OrderViewModel();

  Future<Position> getLocation() async {
    setBusy(true);
    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    setBusy(false);
    return position;
  }

  // Future<bool> isUserTokenValid() async {
  //   setBusy(true);
  //   FirebaseUser currentUser = await _authenticationService.currentUser();
  //   String token = "";
  //   await currentUser.getIdToken(refresh: true).then((user)=> {
  //     token = user.token
  //   });
  //   setBusy(false);
  //   return token != null;
  // }

  // Future<FirebaseUser> login(String email, String password) async {
  //   setBusy(true);
  //   var user = await _authenticationService.signIn(email, password);
  //   setBusy(false);
    
  //   return user;
  // }

}
