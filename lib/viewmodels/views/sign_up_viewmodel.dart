import 'package:doobr_customer/viewmodels/base_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../../core/services/authentication_service.dart';

class SignUpViewModel extends BaseModel {
  AuthenticationService _authenticationService;

  SignUpViewModel(this._authenticationService);

  Future<FirebaseUser> signUp(String email, String password) async {
    setBusy(true);
    var user = await _authenticationService.register(email, password);
    setBusy(false);
    return user;
  }

}
