

import 'package:doobr_customer/core/models/comment.dart';
import 'package:doobr_customer/core/services/api.dart';
import 'package:doobr_customer/viewmodels/base_model.dart';

class CommentsModel extends BaseModel {
  Api _api;
  CommentsModel(this._api);

  List<Comment> comments;

  Future fetchComments(int postId) async {
    setBusy(true);
    comments = await _api.getCommentsForPost(postId);
    setBusy(false);
  }

}