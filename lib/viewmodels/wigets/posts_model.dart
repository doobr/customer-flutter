
import '../../core/models/post.dart';
import '../../core/services/api.dart';
import '../base_model.dart';

class PostsModel extends BaseModel {
  Api _api;

  PostsModel(this._api);
  
  List<Post> posts;

  Future getPosts(int userId) async {
    setBusy(true);
    posts = await _api.getPostsForUser(userId);
    setBusy(false);
  }

}